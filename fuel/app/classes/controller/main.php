<?php

require APPPATH.'vendor/autoload.php';

use Fuel\Core\View;
use Fuel\Core\Controller_Rest;

/**
 * Description of hello
 *
 * @author mauro_000
 */
class Controller_Main extends Controller_Rest
{

    protected $format = 'json';


    /**
    * This method renders the main page
    * Using this page the user will be able to
    * generate the token and teh authorization token
    * as well as access the API
    *
    */
    public function action_index()
    {
        // display an authorization form
        return Response::forge(View::forge('main/index'));
    }

    /**
    * This method is responsible for generating the token
    * User will access the main page and provide all information
    * to generate the token
    */
    public function action_generate_token()
    {
        $server = $this->init_server();

        // Handle a request for an OAuth2.0 Access Token and send the response to the client
        $server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
    }

    /**
    * This method renders the authorization page and also
    * is responsible for generating the authorization code
    * based on the user params sent by POST
    * @access  public
    * @return  OAuth2\Server
    */
    public function action_authorize()
    {
        // when access the url directly, a form is rendered
        if (empty($_POST)) {
            $data['response_type'] = $this->param('response_type');
            $data['client_id']     = $this->param('client_id');
            $data['state']         = $this->param('state');

            // display an authorization form
            return Response::forge(View::forge('main/authorize', $data));
        } else {
            $server = $this->init_server();
            $request = OAuth2\Request::createFromGlobals();
            $response = new OAuth2\Response();

            // validate the authorize request
            if (!$server->validateAuthorizeRequest($request, $response)) {
                return $response->send();
            }

            // print the authorization code if the user has authorized your client
            $is_authorized = ($_POST['authorized'] === 'Yes');
            $server->handleAuthorizeRequest($request, $response, $is_authorized);
            if ($is_authorized) {
                // this is only here so that you get to see your code in the cURL request.
                //Otherwise, we'd redirect back to the client
                $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
                return $this->response(['access_token' => $code]);
            }
            return $response->send();
        }
    }

    /**
    * API
    * This method receives a param from GET method and print it out
    */
    public function post_json()
    {
        $server = $this->init_server();

        // Handle a request to a resource and authenticate the access token
        if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
            return $this->response(['error_description' => 'Fail to athenticate.']);
        }

        return $this->response(['hello' => 'world']);
    }

    /**
    * Initialize the server so the other methods
    * can access the database and insert/check valid tokens
    *
    * @access  protected
    * @return  OAuth2\Server
    */
    protected function init_server()
    {
        $dsn      = 'mysql:dbname=my_oauth2_db;host=localhost';
        $username = 'root';
        $password = '';

        // error reporting (this is a demo, after all!)
        ini_set('display_errors',1);
        error_reporting(E_ALL);

        // $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"
        $storage = new OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $username, 'password' => $password));

        // Pass a storage object or array of storage objects to the OAuth2 server class
        $server = new OAuth2\Server($storage);

        // Add the "Client Credentials" grant type (it is the simplest of the grant types)
        $server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));

        // Add the "Authorization Code" grant type (this is where the oauth magic happens)
        $server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

        return $server;
    }

    /**
    * The 404 action for the application.
    *
    * @access  public
    * @return  Response
    */
    public function action_404()
    {
            return Response::forge(Presenter::forge('welcome/404'), 404);
    }
}
