<!DOCTYPE html>
<html>
    <body>
        <h2>Authorize Client</h2>
        <h3>Please fill out all the fields bellow</h3>
        <h4>
            <a href="/">Home</a>
            <a href="/authorize">Authorization Page</a>
        </h4>
        <?php echo Form::open('/authorize'); ?>

            <?php echo Form::fieldset_open(); ?>
                <?php echo Form::label('Response Type:', 'response_type'); ?>
                <?php echo Form::input('response_type', $response_type, array('type' => 'text', 'id' => 'response_type')); ?>
            <?php echo Form::fieldset_close(); ?>

            <?php echo Form::fieldset_open(); ?>
                <?php echo Form::label('Client ID:', 'client_id'); ?>
                <?php echo Form::input('client_id', $client_id, array('type' => 'text', 'id' => 'client_id')); ?>
            <?php echo Form::fieldset_close(); ?>

            <?php echo Form::fieldset_open(); ?>
                <?php echo Form::label('State:', 'state'); ?>
                <?php echo Form::input('state', $state, array('type' => 'text', 'id' => 'state')); ?>
            <?php echo Form::fieldset_close(); ?>

            <?php echo Form::submit('authorized', 'Yes'); ?>
            <?php echo Form::submit('authorized', 'No'); ?>

        <?php echo Form::close(); ?>
    </body>
</html>