<!DOCTYPE html>
<html>
    <head>
        <?php echo Asset::js(array(
            'http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js',
            'json2.js',
            'custom.js',
        ));?>
    </head>
    <body>
        <h2>Generate Token</h2>
        <h3>Please fill out all the fields bellow</h3>
        <h4>
            <a href="/">Home</a>
            <a href="/authorize">Authorization Page</a>
        </h4>
        <?php echo Form::open(array('action' => '', 'class' => 'token-form')); ?>

            <?php echo Form::fieldset_open(); ?>
                <?php echo Form::label('Client ID:', 'client_id'); ?>
                <?php echo Form::input('client_id', 'testclient', array('type' => 'text', 'id' => 'client_id')); ?>
            <?php echo Form::fieldset_close(); ?>

            <?php echo Form::fieldset_open(); ?>
                <?php echo Form::label('Client Secret:', 'client_secret'); ?>
                <?php echo Form::input('client_secret', 'testpass', array('type' => 'password', 'id' => 'client_secret')); ?>
            <?php echo Form::fieldset_close(); ?>

            <?php echo Form::fieldset_open(); ?>
                <?php echo Form::label('Grant Type:', 'grant_type'); ?>
                <?php echo Form::input('grant_type', 'client_credentials', array('type' => 'text', 'id' => 'grant_type')); ?>
            <?php echo Form::fieldset_close(); ?>

            <?php echo Form::input('authorized', 'Generate Code', array('class' => 'generate-token', 'type' => 'button')); ?>
            <?php echo Form::input('authorized', 'Call API', array('class' => 'call-api', 'type' => 'button')); ?>

        <?php echo Form::close(); ?>

        <table>
            <tr class="tr-token">
                <td>Token generated: </td>
                <td class="td-token"></td>
            </tr>
            <tr class="tr-return-api">
                <td>Return from API: </td>
                <td class="td-return-api"></td>
            </tr>
        </table>
    </body>
</html>