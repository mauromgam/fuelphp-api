<?php
return array(
	'_root_'  => 'main/index',  // The default route
	'_404_'   => 'welcome/404',    // The main 404 route

        'generate_token' => 'main/generate_token',
        'authorize(/:response_type/:client_id/:state)?' => 'main/authorize',

        'hello_api' => 'main/json',
);
