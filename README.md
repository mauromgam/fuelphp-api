All code I've written is placed in /controller/main.php.

---------------------------------------------------------------------------------------------------------------------

Step 3: Create a simple GET request which will return JSON data with this format { “hello”: ”world” }
This step can be tested by accessing the url below using a browser.

fuelphp.local/hello/world

---------------------------------------------------------------------------------------------------------------------

Step 4: Use OAuth2 Server PHP Library to Authenticate the API
The controller /controller/main.php makes use of OAtuh2 classes to generate a token based on a user stored in my local database.
My local database was built from the tutorial on the page below.

http://bshaffer.github.io/oauth2-server-php-docs/cookbook/


Command to generate the token

curl -u testclient:testpass http://fuelphp.local/generate_token -d 'grant_type=client_credentials'


Command to generate the resource using the code provided above

curl -u testclient:testpass http://fuelphp.local/resource -d 'grant_type=authorization_code&code=885f8394e12d699bb27c3d17e2e57550b8cf4f74'


---------------------------------------------------------------------------------------------------------------------

I created a pretty simple layout to generate token and authorization code.

You just need to access the home page, in my case http://fuelphp.local/, and use it.

After provide the user credentials and generate the token, you'll be able to call the API.