$(function(){
    $('.tr-token').hide();
    $('.tr-return-api').hide();

    // POSTs data to Controller_Main::action_generate_token
    $('.generate-token').on('click', function(){
        $.ajax({
            url: '/generate_token',
            data: $('.token-form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                // The access token retrieved will
                // be displayed in the html table.
                // In case of error, it will be displayed
                // instead of the token generated.
                if (typeof data.access_token != 'undefined') {
                    $('.td-token').html(data.access_token);
                } else {
                    $('.td-token').html(
                        '<strong style="color:red">'+data.error_description+'<strong>'
                    );
                }
                $('.tr-token').show();
            },
            error: function(errorThrown) {

            }
        });
    });

    // Call api after generate the token
    $('.call-api').on('click', function(){
        var token = $('.td-token').html().trim();
        if (token != '') {
            // POSTs the access_token to the API
            // to get the API's content
            var request_data = {
                "access_token": token
            }
            $.ajax({
                url: '/hello_api',
                type: 'POST',
                data: request_data,
                dataType: 'json',
                success: function(data) {
                    var json_string = JSON.stringify(data);
                    $('.td-return-api').html(json_string);
                },
                error: function(errorThrown) {

                }
            });
        } else {
            $('.td-return-api').html(
                '<strong style="color:red">Please generate the token first.<strong>'
            );
        }
        $('.tr-return-api').show();
    });
});